import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="weatherimerg",
    version="0.0.1",
    author="WeatherForce",
    author_email="contact@weatherforce.org",
    description="package for imerg project on CIV",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="git@gitlab.com:ibenlems/weather-imerg.git",
    packages=setuptools.find_packages(exclude=["tests", "tests.*"]),
    classifiers=(
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ),
)

# weatherimerg

package for imerg project on CIV

## Install

### On local, using a virtual environment
Create the virtualenv
```bash
python3 -m virtualenv <your-env>
```

Activate it, and install dependencies
```bash
source <your-env>/bin/activate
pip install -r requirements.txt
```
And now you can install your package. For normal usage:

```bash
pip install .
```
For development usage:

```bash
pip install -e .
```
### On the hub
install dependencies
```bash
pip install --user -r requirements.txt
```
install your package. For normal usage:

```bash
pip install --user .
```
For development usage:

```bash
pip install -e --user .
```

## Use

### Develop and try it out

You can use every kind of editor to edit your package.
once you have edited your code, you can try it at notebooks/How_to_use.ipynb.
It will have already imports your package.

### Lint it

Your code needs to be Pep8 compliant. To do so there are various tools, but we have provided  one called Flake8

```bash
flake8 weatherimerg
```
this tool will check out all of your code, and point you to the lines that do not comply with PEP 8.
more [on flake 8](https://flake8.pycqa.org/en/latest/).

> Note: if you want a tool that automagically make your code PEP 8 compliant, you can have a look at [autopep8]('https://pypi.org/project/autopep8/') and [black](https://github.com/psf/black), which are tools toformat your code.



## Test

By default, python provide a unittest package, which allows you to create unit tests. Unit tests are a way for you to test pieces of your code by scripting a process ( within your unit test ) where you give inputs to one of your function you want to test, make it run with it, collect outputs, and assert that this output matches your expectations. You can have a look on the unit test example in the tests folder.

To launch unit testing, hit the following command:

```bash
python -m unittest
```

This will launch all unit tests that are presents in tests/ folder.

Here is a documentation about it: [unittest documentation](https://docs.python.org/fr/3/library/unittest.html).

## Versioning

If you want to version it with git, go to gitlab account, click on the new repository button, and follow the instructions in the "already existing folder part".


### git commands you should know

to have an up to date version of code, pull sources from gitlab using:

```bash
git pull
```

to add your changes into a commit:

```bash
git add <file>
```

to commit :

```bash
git commit -m "<insert your commit message here>"
```
and to push on gitlab

```bash
git push
```

### Ignoring some files or folder

If you want to ignore some specific files or folders, you can add an entry into .gitignore file. You can have a look at this file in the command line using either cat command, or a text editor such as nano or vim, like this :

```bash
cat .gitignore
```

for example, if you want to ignore a file <some-file>.<some-extension>  you can append it to the end of your .gitignore file using the command line, or the text editor of your choice.
find below a bash command that appends to gitignore file <some-file>.<some-extension>, and then print the tail of the gitignore file, for you to verify that it actually did append the file to .gitgnore.

```bash
echo <some-file>.<some-extension> >> .gitignore
tail .gitignore
```
Git understands regular expressions to ignore files, that means that you can rule out every file having an identical pattern in their name. For example, to rule out every files that has a specific file extension, append `*.<specific-extension>` to your .gitignore.
More on that subject in [git cheatsheet](https://github.github.com/training-kit/downloads/fr/github-git-cheat-sheet/).  

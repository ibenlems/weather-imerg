import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
from scipy.ndimage.filters import gaussian_filter
from os import listdir , walk
import os
import h5py as h5
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib
import imageio
import netCDF4 as nc 
import datetime
import plotly.express as px
from tqdm import tqdm



def get_df_from_hdf5(fname):
    """get an xarray Dataset from an hdf5 file
    param :
      fname : the hdf5 file path """
    f=h5.File(fname,'r')
    """
    For files stored as HDF5.
    We need to read them in netcdf first.
    """
    ds = nc.Dataset(fname,mode="r")
    df = xr.Dataset()
    for var in list(dataset['Grid'].keys()):
        
        
        if (var != "lon" and var!= "lat" and var != "lat_bnds" and var!= "lon_bnds" and var != "latv" and var != "lonv" and var!="nv" and var!="time"and var!="time_bnds"):
            r=ds.groups["Grid"].variables[var][:]
            df[var] = (("lon","lat"),r)
        
    df.coords["lat"] = (("lat"),ds["Grid"]["lat"][:])
    df.coords["lon"] = (("lon"),ds["Grid"]["lon"][:])
    df.coords["time"] = (("time"),ds["Grid"]["time"][:])
    

    #df.transpose("lat","lon")
    return df

def get_precip_data_from_hdf5(fname):
    
    """get only the precipitation data from an hdf5 file
    param:
        fname : the path of the hdf5 file
        return xarray data set corresponding to precipitation variable"""
    f=h5.File(fname,'r')
    ds = nc.Dataset(fname,mode="r")
    df = xr.Dataset()
    #for var in list(dataset['Grid'].keys()):
    #if var != "lon" and var!= "lat" and var != "lat_bnds" and var!= "lon_bnds" and var != "latv" and var != "lonv" and  #var!="nv" and var!="time"and var!="time_bnds":
    r=ds.groups["Grid"].variables['precipitationCal'][:]
    df['precipitationCal'] = (("lon","lat"),r)
        
    df.coords["lat"] = (("lat"),ds["Grid"]["lat"][:])
    df.coords["lon"] = (("lon"),ds["Grid"]["lon"][:])
    df.coords["time"] = (("time"),ds["Grid"]["time"][:])
    
    return df  

def create_df_day(path):
    """create a dataset for one day 
    param:
        path : the path of the day directory"""
    
    datasets=[]
    for r, d, f in os.walk(path):
        for file in sorted(f):
            #print(file)
            #files.append(file)
            x=os.path.join(r, file)
            df = get_df_from_hdf5(x)
            datasets.append(df)

    ds=xr.concat(datasets,dim='time')
    ds=ds.transpose("time","lat","lon")
    return ds

def get_precipitation_data_day(path):
    #path='/home/jovyan/data/satellite/IMERG/Year/'+Month+'/'+Day+'/'
    datasets=[]
    for r, d, f in os.walk(path):
  
        for file in sorted(f):
            #print(file)
            #files.append(file)
            x=os.path.join(r, file)
            df = get_precip_data_from_hdf5(x)
  
            datasets.append(df)
          

    ds=xr.concat(datasets,dim='time')
    ds=ds.transpose("time","lat","lon")
    #ds.coords['date']=datetime.datetime(2019,int(Month),int(Day))
    return ds   

def list_df_month(path):
    #directories=[]
    dfs=[]
    for r, d, f in os.walk(path):
        iter=1
        for dire in sorted(d):
            #directories.append(dire)
            thepath=os.path.join(path, dire)
            print(thepath)
            print(iter)
            df=create_df_day(thepath)
            dfs.append(df)
            print(iter)
            iter +=1 
    return dfs

def is_valid(str):
    return str  in ['01','02','03','04','05','06','07','08','09','10','11','12','11','12','13', '14','15','16',
                    '17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']

def list_path(main_path):
    """return a list of directories in the main path"""
    
    liste=[]
    for dire in sorted(listdir(main_path)):
                #directories.append(dire)
            if is_valid(dire):
                
                thepath=os.path.join(main_path, dire)
                liste.append(thepath)
    return liste
            
def list_files(main_path):
    "return a list of file names in the main path"
    
    liste=[]
    for r, d, f in os.walk(main_path):
            
            for file in sorted(f):
                #directories.append(dire)
                thepath=os.path.join(main_path, file)
                liste.append(thepath)
    return liste

def get_dfs_month(path):
    "return a list of dataset for the precipitation variable for each day for the month given in the path"
    
    datasets=[]
    for path in paths:
        
        dt=get_precipitation_data_day(path)
        #l=path.split('/')
        #n=len(l)
        #dt.to_netcdf('./'+l[n-2]+'/'+l[n-1]+'.nc')
        datasets.append(dt)
    return datasets

def create_ds_month(path):
    """create a dataset for each day of the month given in the path and store it as netcdf file
    it only takes precipitation variable but could easily be changed to take more variables or even
    the whole dataset"""
    paths=list_path(path)
    #datasets=[]
    t=tqdm(paths,desc='Month: ')
    for path in t:
           
        
        dt=get_precipitation_data_day(path)
        l=path.split('/')
        n=len(l)
        dt.coords['date']=datetime.datetime(int(l[n-3]),int(l[n-2]),int(l[n-1]))
        t.set_description("Month:"+l[n-2])
        try:
            os.mkdir('./IMERG/precipitation/'+l[n-3]+'/'+l[n-2])
        except FileExistsError:
            None

        dt.to_netcdf('./IMERG/precipitation/'+l[n-3]+'/'+l[n-2]+'/'+l[n-1]+'.nc',compute=False)
        #print("Month : "+l[n-2]+", Day: "+l[n-1] + " created")
        #datasets.append(dt)
    
        
        
def create_imerg_precp_ds(year):
    """create dasaets in netcdf format for the precipitation variable and store them in IMERG/precipitation"""
    paths=list_path('/home/jovyan/data/satellite/IMERG/'+year+'/')
    
  
    for path in paths:
        create_ds_month(path)
    
    
    
def get_ds_date_Month(Year,Month):
    """create daily precipitation data for one month (sum over time dimension )"""
    
    path='./IMERG/precipitation/'+Year+'/'+Month+'/'
    files=list_files(path)
    dss=[]
    for f in files:
        ds=xr.open_dataset(f)
        l=f.split('/')
        day=l[len(l)-1].split('.')[0]
        dd=ds.sum(dim='time')

        dd.coords['date']=datetime.datetime(int(Year),int(Month),int(day))
        dss.append(dd)    
    return dss


def create_ds_one_month(Year,Month):
    """create a daily precipitation data for the month and store it in a netcdf file"""
    
    Month_ds=get_ds_date_Month(Year,Month)
    Month_t = xr.concat(Month_ds,dim='date')
  
    try:
        os.mkdir('./IMERG/precipitation_date/'+Year)
    except FileExistsError:
        None
  
    Month_t.to_netcdf('./IMERG/precipitation_date/'+Year+'/'+'Monthly_'+Month+'.nc',compute=False)
    
    
def create_ds_Monthly(Year):
    months=['01','02','03','04','05','06','07','08','09','10','11','12']
    t=tqdm(months)
    for m in t:
        t.set_description("Month:"+m)
        """create a daily precipitation data for the month and store it in a netcdf file"""
        create_ds_one_month(Year,m)
        #print("Months "+m+" created")
    
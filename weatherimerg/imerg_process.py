
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
from scipy.ndimage.filters import gaussian_filter
from os import listdir , walk
import os
import h5py as h5
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib
import imageio
import netCDF4 as nc 
import datetime
import plotly.express as px
from sklearn.metrics import mean_squared_error, r2_score,mean_absolute_error
import statsmodels.api as sm
import pylab as py 
import scipy.stats as stats
from tqdm import tqdm



def get_lon_lat_observ(name):
    "get the latitude and longitude of the station form metadata"
    """name could be         ODIENNE
                              KORHOGO
                       FERKESSEDOUGOU
                       BONDOUKOU/SOKO
                                  MAN
                               BOUAKE
                               GAGNOA
                                DALOA
                        DIMBOKRO CITY
                         YAMOUSSOUKRO
  ABIDJAN FELIX HOUPHOUET BOIGNY INTL
                               ADIAKE
                                TABOU
                            SAN PEDRO
                      SASSANDRA/DREWIN"""
    observ_history=pd.read_csv('/home/jovyan/data/observations/gsod/isd-history.csv')
    lat= observ_history.loc[observ_history['STATION NAME']==name].iloc[0][6]
    lon= observ_history.loc[observ_history['STATION NAME']==name].iloc[0][7]
    return lon,lat

def get_observ(name,years):
    results=[]
    for year in years:
        
        """get observation data for the station specified by name

         name could be         ODIENNE
                                  KORHOGO
                           FERKESSEDOUGOU
                           BONDOUKOU/SOKO
                                      MAN
                                   BOUAKE
                                   GAGNOA
                                    DALOA
                            DIMBOKRO CITY
                             YAMOUSSOUKRO
      ABIDJAN FELIX HOUPHOUET BOIGNY INTL
                                   ADIAKE
                                    TABOU
                                SAN PEDRO
                          SASSANDRA/DREWIN"""

        metadata=pd.read_pickle('/home/jovyan/data/observations/gsod/station_metadata.pkl')
        CIV_data = metadata.loc[metadata["ctry"]=='IV'].astype({'id': 'int64'})
        observations=pd.read_pickle('/home/jovyan/data/observations/gsod/gsod_'+year+'.pkl')
        observ=observations.astype({'id': 'int64'})
        ide = CIV_data.loc[CIV_data['station name']==name].iloc[0][0]

        observ_name=observ.loc[observ['id']==ide]
        observ_precp_a= observ_name[['date','prcp']]

        observ_precp=observ_precp_a.set_index('date')

        observ_precp=observ_precp.to_xarray()
        results.append(observ_precp)
    res=xr.concat(results,dim='date')
    return res


def combine_observ_pred(pred,observ):
    
    result = pd.concat([pred, observ], axis=1, sort=False)
    
    result=result[['precipitationCal','prcp']]
    result=result.rename(columns={"precipitationCal":"imerg_precp","prcp": "observed precp"})
    return result

def compute_errors(d):
    y_pred = d.imerg_precp
    y_true = d['observed precp']
    MSE = mean_squared_error(y_true, y_pred)
    MAE = mean_absolute_error(y_pred,y_true)

Jupyter Notebook
imerg_process.py
5 minutes ago 

Python

    File
    Edit
    View
    Language


    Variance = np.var(y_pred)
    SSE = np.mean((np.mean(y_pred) -y_true)** 2) # Where Y is your dependent variable. # SSE : Sum of squared errors.

    Bias = (SSE - Variance)/Variance
    return MAE, MSE , Bias


def slice_data_to_seasons(data,year):
    
    
    """Slicing the dataset to get four different seasons
    param: data the dataset to slice
    return: a list of datasets correspending to four different seasons"""

    slice_data1 = data.sel(date=slice(year+'-01', year+'-02'))
    slice_data2 = data.sel(date=slice(year+'-03', year+'-05'))
    slice_data3 = data.sel(date=slice(year+'-06', year+'-08'))
    slice_data4 = data.sel(date=slice(year+'-09', year+'-12'))
    slices=[slice_data1,
    slice_data2, 
    slice_data3, 
    slice_data4 ]
    return slices

def plot_qq_seasons(data):
    """plot quantile to quantile data for four seasons"""
    slices=slice_data_to_seasons(data)
    

    fig = plt.figure(figsize=(10,10))
    i=1
    titles=['jan-feb','march-may','june-aug','sept-dec']
    for sl in slices:

        ax = fig.add_subplot(2, 2, i)
        mod_fit = sm.OLS(sl.to_dataframe().imerg_precp,sl.to_dataframe()['observed precp']).fit()
        res = mod_fit.resid # residuals
        sm.qqplot(res,line='45',fit=True,ax=ax)
        plt.xlabel("Imerg prediction")
        plt.ylabel("Observation")
        plt.title(titles[i-1])

        i+=1
        
def plot_qq(data):
    """ plot quantile to quantile data """
    mod_fit = sm.OLS(data.imerg_precp,data['observed precp']).fit()
    res = mod_fit.resid # residuals
    fig = sm.qqplot(res,line='45',fit=True)
    plt.xlabel("Imerg prediction")
    plt.ylabel("Observation")
    py.show()
    
def get_observ_pred(station_name,years):
   
    """get the obseration and prediction data in one dataset
       get_observ_pred(station_name,year)
           name could be         ODIENNE
                              KORHOGO
                       FERKESSEDOUGOU
                       BONDOUKOU/SOKO
                                  MAN
                               BOUAKE
                               GAGNOA
                                DALOA
                        DIMBOKRO CITY
                         YAMOUSSOUKRO
                          ABIDJAN FELIX HOUPHOUET BOIGNY INTL
                               ADIAKE
                                TABOU
                            SAN PEDRO
                      SASSANDRA/DREWIN"""

   

    observ_fer = get_observ(station_name,years)
    fer_lon,fer_lat=get_lon_lat_observ(station_name)
    my_data=get_imerg_data(years,None)
    my_data=my_data.sel(lon=fer_lon,lat=fer_lat,method="nearest")
    #imerg_fer=my_data.to_dataframe().reset_index().drop(['lat', 'lon'], axis=1).set_index('date')
    fer = combine_observ_pred(observ_fer.to_dataframe(),my_data.to_dataframe())
    return fer  
    
def get_imerg_data(years,WNES):
    """ WNES=[West(min lon),North(max lat),East(max lon),South(min lat))]
    WNES could be None if we want the whole dataset"""
    results=[]
    t=tqdm(years)
    for year in t:
        my_data=xr.open_mfdataset('./IMERG/precipitation_date/'+year+'/*.nc',combine='by_coords')

        if WNES != None:
             my_data=my_data.loc[dict(lon=slice(WNES[0], WNES[2]),lat=slice(WNES[3],WNES[1]))]
        t.set_description("year:"+year)

        results.append(my_data)
    res=xr.concat(results,dim='date')
    return res
        
def get_ds_by_date(Year,Month,Day):
    #returns the imerg precipitation data for the Month and day given in the path for the year 2019
    path='./IMERG/precipitation/'+Year+'/'+Month+'/'+Day+'.nc'
    ds=xr.open_dataset(path)
    return ds
   
def plot_precipitation(date,data):
    # Data for plottingh
    """plot the precipitation data in time t over a map
    param:
        path : the path of the data to plot just to set the right name for the figure
        data: the dataset to plot
        t time dimension of the data to plot"""
    data=data.transpose('lon','lat')
    sub_data=data.precipitationCal

    fig, ax = plt.subplots(figsize=(20,10))
    
    sst=sub_data.T
    lats = data.lat
    lons = data.lon
    data = gaussian_filter(sst, 0.7)
    ax = plt.axes(projection=ccrs.PlateCarree())
    ax.add_feature(cfeature.BORDERS)#, linestyle='.')

    CS = plt.contourf(lons, lats, data, 
             transform=ccrs.PlateCarree())
    fig.colorbar(CS, ax=ax, shrink=0.9)
    ax.coastlines()
    #plt.imshow(sub_data)
    
    ax.set_title('date: '+date)
    #sub_data.plot(ax=ax)
    

    # IMPORTANT ANIMATION CODE HERE
    # Used to keep the limits constant
    #ax.set_ylim(0, y_max)

    # Used to return the plot as an image rray
    fig.canvas.draw()       # draw the canvas, cache the renderer
    image = np.frombuffer(fig.canvas.tostring_rgb(), dtype='uint8')
    image  = image.reshape(fig.canvas.get_width_height()[::-1] + (3,))
    #plt.close(fig)

    #return image
    
def make_map_with_features(WNES=[-20, 45, 60, -40], figsize=(12,6),
                           projection=cp.crs.PlateCarree(),
                           colorbar=True,
                           label_fontdict=None,
                           land=False, ocean=False, coastline=True,
                           borders=True, lakes=False, rivers=False,
                          ):
    """
    Make a figure with subplot that contains a map with optional features

    Returns the figure, axis, and optional colorbar axis
    """
    fig, ax = plt.subplots(1, 1, figsize=figsize,
                           subplot_kw={'projection' : projection});

    if colorbar:
        divider = make_axes_locatable(ax)
        cbar_ax = divider.new_horizontal(size="5%", pad=0.1, axes_class=plt.Axes)
        fig.add_axes(cbar_ax)
    
    ax.set_extent([WNES[0], WNES[2], WNES[3], WNES[1]], crs=projection);
    glines = ax.gridlines(crs=projection, draw_labels=True, linewidth=1,
                          color='k', linestyle='-', alpha=0.);
    glines.xlabels_top = False;
    glines.ylabels_right = False;
    glines.xformatter = cp.mpl.gridliner.LONGITUDE_FORMATTER;
    glines.yformatter = cp.mpl.gridliner.LATITUDE_FORMATTER;
    
    label_fontdict = (label_fontdict or 
                      {'size': 12, 'color': 'black', 'weight': 'medium'})
    glines.xlabel_style = label_fontdict
    glines.ylabel_style = label_fontdict

    if(land):      ax.add_feature(cp.feature.LAND);
    if(ocean):     ax.add_feature(cp.feature.OCEAN);
    if(coastline): ax.add_feature(cp.feature.COASTLINE);
    if(borders):   ax.add_feature(cp.feature.BORDERS, linestyle='--');
    if(lakes):     ax.add_feature(cp.feature.LAKES, alpha=0.5);
    if(rivers):    ax.add_feature(cp.feature.RIVERS);
    if colorbar:
        return fig, ax, cbar_ax
    else:
        return fig, ax